var map = L.map('map').setView([32.835, -96.72], 13)

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	maxZoom: 18,
	attribution: 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
}).addTo(map);

L.tileLayer('/white_rock_lake/{z}/{x}/{y}.png', {
	maxZoom: 18,
	attribution: 'Allan Adair'
}).addTo(map);
