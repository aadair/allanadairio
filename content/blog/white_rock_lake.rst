White Rock Lake
===============
:date: 2015-02-23
:JavaScripts: http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js, white_rock_lake_map.js
:Stylesheets: http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css
:tags: running, gis, gpx, gps, mapnik, garmin, leaflet
:Status: published

I went running around White Rock Lake last weekend. This area is known as one
of the nicer places to experience the outdoors in Dallas.

It might be winter in the northern hemisphere, but the weather in this part of
Texas is very irregular. At the time of running, it was a balmy 19° C (66° F).
More or less ideal, I'd say. Except for the bird feces, which drowns out a
solid kilometer or two near the end of my run. That's not snow or ice in the
below image.

.. image:: images/white_rock_bird_shit.jpg
    :alt: poopy trees
    :class: blog-img

This was an opportunity to test out a new Garmin watch that I have been
eyeballing for a while. The Garmin Forerunner 920XT is one of two watches that
I know can receive GLONASS signals along with GPS.

.. image:: images/garmin_watch.jpg
    :alt: Garmin Forerunner 920XT
    :class: blog-img

Besides the desire for general fitness and outdoor enjoyment, I've had this
idea for a little while about this static blog and how it could potentially
serve out custom map tiles. I decided to use data from my run. Here's a simple
map application showing the geographic data collected from the watch:

.. raw:: html

    <div id="map" style="width: 100%; height: 400px"></div>

Getting the data to this point consisted of a few steps:

1. Getting the data off of the watch, and in a usable format.
2. Generating map tiles to overlay an existing basemap (here I am using
   OpenStreetMap as a basemap).
3. Building a simple leaflet application for the presentation layer.

There was also a bit of trial and error to get it to play nicely with my static
blog. For anyone interested in how it came to this, I have already posted
sample data and sourcecode on GitHub: https://github.com/allanadair/maptiles-testing

I also plan on writing some content with greater detail in the `future
</rendering-map-tiles-with-mapnik.html>`_.
