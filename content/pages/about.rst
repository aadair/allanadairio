About
=====
:Status: published

I'm a senior GIS developer and system architect with extensive experience in
developing, deploying, and maintaining enterprise geospatial solutions.

I have a colorful history in software development, and it's one of my passions.
In total, I have over 15 years of software development experience and I have
been described by others as a full-stack developer. I love programming, and I
enjoy learning new languages and development paradigms. I have proficiency in
multiple programming languages, and I strongly embrace the mantra of using the
right tool for the job.

I happen to be one of the rare lucky people in the world whose hobbies and
interests intersect with their professional lives. Just as a professional
musician might enjoy performing without pay for friends and family, I also
enjoy reading up on the latest trending technologies in GIS, hacking away at a
few open source solutions, and keeping up to date on the latest software
development practices.

I have a lot of balance in my life and I do not work to the point of burn-out.
Outside of being a total nerd with GIS and software development, I'm big into
cycling, running, and skiing. I keep a pretty nice herb garden. I have a deep
appreciation for travel and experiencing different cultures, and I seek out
opportunities that allow for these types of enriching experiences.

|
| Best regards,
|
|  Allan Adair
|  allan.m.adair@gmail.com
