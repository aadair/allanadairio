Miscellaneous Code
==================
:Status: published

`wmatz <https://github.com/allanadair/wmatz>`_ (`blog post
<http://allan.adair.io/calculating-weighted-geometric-median-of-3d-points.html>`_)
serves as an example methodology for calculating the weighted geometric
median of 3D points. A variant of this code was used for locating RFID tags
in 3D space from sensor data.

`ode <https://github.com/allanadair/ode>`_ is a demo Flask app with simple
geospatial querying. 

`codetest <https://github.com/allanadair/codetest>`_ is a quick and dirty Flask
app showcasing some very basic CRUD operations. It runs on both Python 2 and
Python 3.

`awsutils <https://github.com/allanadair/awsutils>`_ (`PyPI
<https://pypi.python.org/pypi/awsutils/1.0.7>`_) represents a grab bag of
utilities that make life easier when working with amazon web services.

`kmzgen <https://bitbucket.org/aadair/kmzgen>`_ is an ArcGIS geoprocessing
service for creating custom Garmin maps (the KMZ variety) from ArcGIS tiled map
services. It was written a few years ago for the Garmin distributor in Norway,
GeoInsight.

`shapemaker <https://github.com/allanadair/shapemaker>`_ is an ArcGIS
geoprocessing service that receives
ArcGIS-flavored geojson, builds and zips a shapefile containing intersected
features, and returns a URL. This was specific to a project, but has some
useful examples of Python toolbox structure and other methods for writing
clean-ish Python software.

`osmbuildingextractor <https://github.com/allanadair/osmbuildingextractor>`_ is
a snippet for extracting building footprints from OSM data.
