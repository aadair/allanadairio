Curriculum Vitae
================
:Status: published

| Allan Adair
| E-mail: allan.m.adair@gmail.com
| Personal website: http://allan.adair.io
| LinkedIn: https://www.linkedin.com/in/allan-adair-06b07511
|

Summary
-------
| Senior GIS developer and system architect with extensive experience developing,
  deploying, and maintaining enterprise geospatial solutions.
|

**Highlights**:
  - Love solving complex problems involving concurrency, graph theory, indoor
    mapping, and spatial analysis
  - Professionally trained in both Scrum and Kanban
  - Seasoned Python developer with lots of experience (15+ years)
  - Expert-level knowledge of relational data modeling and SQL
  - Competent in JavaScript, Dojo, and ArcGIS Web AppBuilder
  - Experienced with other languages: C, C++, C# and Silverlight
  - Interested in working with: Rust, Julia, R, octave
  - Experienced in using cloud providers: Amazon, Azure, Heroku
  - Version control systems: Git, Mercurial, SVN, CVS 
  - In-depth knowledge and proven enterprise experience with the entire ESRI
    ArcGIS stack: Desktop, SDE, Server, Portal, ArcObjects, arcpy
  - Also a heavy user of other enterprise GIS components: MapServer, GeoServer,
    TileStache, PostGIS, Spatialite, SQL Server spatial, Oracle spatial, Rtree,
    Fiona, Shapely, LAS Tools, PDAL, GDAL/OGR toolset, Mapnik toolset, and others
  - Operating systems: Windows, Linux, BSD, and others
  - Virtualization/container technologies: VMware, Citrix, VirtualBox, Vagrant,
    Chef, Docker

Education
---------
- | **M.S. Geospatial Information Science**, University of Texas at Dallas (2009)
  | Thesis: "Optimal Route Selection Based on Analysis of Multiple Criteria Using
    PostGIS and pgRouting"
  |

- | **B.A. Geography**, University of Texas at Dallas (2008)
  | Specialty areas: geospatial analysis, GIS software development, database
    administration, network optimization

Training and Certificates
-------------------------
- Building Applications Using the ArcGIS API for JavaScript (ver 10.2)
- Object-Oriented JavaScript Essentials (Accelebrate)
- Applied Imagery Quick Terrain Modeler
- Agile training
- Implementing ArcGIS Server Geoportal Extension
- Job Tracking for ArcGIS (JTX)
- Cartographic Design Using ArcGIS 9
- Understanding Map Projections and Coordinate Systems
- Creating and Integrating Data for Natural Resource Applications
- Creating and Editing Geodatabase Features with ArcGIS Desktop
- Creating, Editing, and Managing Geodatabases for ArcGIS Desktop
- Working with Geodatabase Subtypes and Domains
- Working with Map Topology in ArcGIS
- Graduate Certificate in Geographic Information Systems from the University of Texas at Dallas

Career History
--------------
- | **GIS Engineer** *(Apr 2015 - Present)*, RFspot Inc
  |    `RFspot <http://rfspot.com/>`_ is a Series A startup that
       I have been contributing to since April of 2015. For them I have been
       architecting an enterprise geospatial system using ArcGIS Server on Linux
       in Amazon EC2, PosgreSQL/PostGIS on Amazon RDS, and other critical
       components in the Amazon Cloud infrastructure. I am the lead Python
       developer for a cloud-based data processing pipeline that produces
       meaningful analytics about retail environments from telemetry data.
       The system is designed to scale in real time by utilizing Celery and
       RabbitMQ as a distributed task queue for delegating tasks to worker
       nodes.
  |
- | **Senior GIS Database Engineer** *(Nov 2013 - Apr 2015)*, Vistronix Inc
  |     For a second contract with the same client within the USDA, I was once
        again responsible for providing highly scalable solutions for ArcGIS
        Server & SDE. Much of my time was spent developing new geoprocessing
        services written in Python and writing JavaScript based client
        applications leveraging the latest from the ArcGIS and Dojo JavaScript
        APIs. I also created some organization-specific tools for deploying
        ArcGIS services in network-isolated production environments.
  | **Reason for leaving:** Opportunity to work in silicon valley for an
    indoor mapping and robotics company was very exciting and compensation
    was alluring enough for me to seize the opportunity.
  |
- | **System Consultant** *(Mar 2011 - Nov 2013)*, Geodata AS
  |     `Geodata AS <http://geodata.no/>`_ is a Norwegian IT company and official
        ESRI software distributor. I was a lead developer for Python and Microsoft
        C# / Silverlight mapping products developed for forestry, oil & gas,
        and utility companies. I collaborated with teams of various skills and
        cultural backgrounds to develop successful products.
  | **Reason for leaving:** A very good opportunity arose with my previous client
    (USDA) that warranted my return.
  |
- | **ArcGIS Server Administrator / Developer** *(Jun 2009 – Mar 2011)*, Paradigm
    Systems GIS Inc
  |     During this time period I was hired for my first contract with the USDA.
        I was responsible for administration, customization, and maintenance of
        multi-tier ArcGIS Server, Desktop, and SDE implementations. Essential
        duties included developing geospatial web services and ArcGIS Desktop
        solutions (.NET), ensuring system availability, and in-depth performance
        monitoring.
  | **Reason for leaving:** Found an excellent opportunity to work for a company
    in Norway and I could not possibly turn it down.
  |
- | **Scientist II** *(Apr 2009 - Jun 2009)*, Dynamac Corporation
  |     Dynamac Corporation is a provides comprehensive environmental, safety &
        health services and robust scientific support capabilities for both
        private and public sector clients. For them I was a contractor and
        technical expert brought on temporarily for the completion of a series
        of cartographic products for Phase I and Phase II environmental site
        assessments.
  | **Reason for leaving:** This was a 3-month contract that was fulfilled.
  |
- | **GIS Technician** *(Jul 2007 - Apr 2009)*, LopezGarcia Group / URS Corporation
  |     URS Corporation (formerly United Research Services) is an engineering,
        design, and construction firm and a U.S. federal government contractor.
        For URS I was tasked with responsibilities ranging from GIS software
        development to the fine-tuning of ArcMap output for professional
        cartographic display. Regular activities included direct client
        interaction, GPS fieldwork, statistical analysis, database modeling, and
        planning GIS projects.
  | **Reason for leaving:** Department layoffs following URS acquisition of
     LopezGarcia Group.
  |
- | **Server** *(Sep 2001 - Jan 2007)*, Texas Roadhouse LLC
  |     For this company I was a dedicated part-time employee working in the
        food service industry while pursuing a formal education in geospatial
        sciences.
  | **Reason for leaving:** Needed more time to dedicate to upper-level
    study.

Project History
---------------
| **rfspotlib**, RFspot Inc
| rfspotlib is a Python package that contains a library and utilities for
  working with RFSpot's data. The package is written in Python and primarliy
  handles the mapping components of telemetric data from a robotic sensor
  platform. It also houses the code used for scaling in Amazon EC2 and
  monitoring distributed tasks. It's comprehensively documented using
  Sphinx-style docstrings and restructured text documents.
|

| **HD Report**, RFspot Inc
| HD Report is a QA reporting system primarily written in Python using the
  Flask framework. It is a collection of software that works in concert to
  provide reports on the accuracy of retail shelf mapping and visual
  confirmation of an AI image processing system.
|

| **rfspot_processing**, RFspot Inc
| rfspot_processing is a Python package that provides tools for interfacing
  with a third party artifical intelligence image processing system. It's
  written in Python 3 and utilizes asyncio for single-threaded concurrent code
  using coroutines.
|

| **rfsgistack**, RFspot Inc
| rfsgisstack is a series of bootstrap scripts written in Python for bringing
  up a customized GIS stack in Amazon EC2. It was created in lieu of ESRI
  providing working Chef scripts configurations compatible with Amazon.
|

| **GeoObserver for Elevation**, National Geospatial Center of Excellence
  (*USDA/Vistronix Inc*)
| GeoObserver for Elevation is a full-stack modular web application and set of
  geoprocessing services written using the ArcGIS JavaScript API, Python, and
  ArcGIS Server. The project represents the evolution of an earlier project
  (GeoProcessor for Elevation) that allows users to generate and download
  custom elevation products on the fly. The front end is written in JavaScript
  and the Dojo framework, with modular dijits that are reusable in other
  applications. The back end is powered by a series of geoprocessing services
  written in Python and hosted by ArcGIS Server and SQL Server.
|

| **NRCS Geodata States Bulk Publishing**, National Geospatial Center of
  Excellence (*USDA/Vistronix Inc*)
| The NRCS Geodata States Bulk Publishing toolbox is a set of tools written in
  Python that enable the packaging and publishing of all services hosted by an
  ArcGIS Server instance. It's used for transferring and deploying services in
  disconnected, secure network environments. 
|

| **Geodata States GP**, National Geospatial Center of Excellence
  (*USDA/Vistronix Inc*)
| Geodata States GP is an asynchronous geoprocessing service written in Python
  designed to provide "Clip and ship" functionality for the geodata states map
  services. Allows granular control over which datasets are available and
  provides an easy to use REST interface through ArcGIS Server.
|

| **GeoSkog**, Geodata AS
| GeoSkog is statistical software used for forest growth predictions and
  economic value estimations relevant to the forestry/silviculture industry in
  Norway. Lead developer role included software architecture & design, guiding
  junior developers, distributed version control system management, and
  hands-on refactoring/development. Project has received positive press in the
  Norwegian media and has been presented at international conferences.
|

| **Lommekjent.no**, Geodata AS
| "Lommekjent", roughly translated as "familiar pocket", is a large-scale
  project for allowing people to map-out recreational trips in Norway. The
  services allow for both preliminary planning of trips, and sharing completed
  trips in a social way. Highlights include being responsible for accumulating
  existing disparate datasets and building topologically-correct network graph
  structures and designing custom algorithms for measuring connectivity and
  exposing disconnected sub-graphs. Documented workflows/activities for
  automated data dumps from yearly updates.
|

| **Statskog Arronderingssalg**, Geodata AS
| Statskog is a state-owned enterprise with a history back to 1860. Principal
  activities are forestry and land-use management on behalf of the Norwegian
  government. The "Arronderingssalg" application aids in the sale,
  classification, and quality assurance of properties' spatial and tabular
  data. The application is Silverlight-based and tightly coupled with Microsoft
  SharePoint and ArcGIS technology. Responsibilities include the development of
  server-side business logic exposed through Python ArcGIS geoprosessing
  services, SharePoint integration using the Silverlight Client Object Model,
  and C# Silverlight development.
|

| **Geodata Online**, Geodata AS
| Geodata Online is a collection of cloud-based ArcGIS services made available
  to Geodata's clients. Assisted in architecture planning of an Amazon-based
  cloud solution for Geodata and GeoInsight, developed utilities for health
  monitoring and maintaining timely responses for geoprocessing services, and
  created scripts for billing service usage to customers.
|

| **Riksantikvaren Askeladden**, Geodata AS
| A major cultural heritage project for the country of Norway. Played a vital
  role by revitalizing country-wide data previously designated as "damaged"
  using low-level geometry operations and tabular relationships. Developed a
  series of server-side ArcGIS geoprocessing services vital to the
  Silverlight-based client application.
|

| **GIS Server Administration**, National Cartography and Geospatial Center
  (*USDA/Paradigm Systems GIS Inc*)
| Architected enterprise-wide geospatial systems following principles of
  systems development lifecycle (SDLC) standards. Configured and administered
  ArcGIS Server and SDE deployments on physical and virtual machines by
  applying regular system updates, patches, optimizations, and customizations.
  Wrote software to monitor availability and performance of ArcGIS Server
  services. Successfully deployed highly scalable GIS services on the Amazon
  EC2 Cloud infrastructure. Optimized map services by using tile caches,
  generalized vector data, and scale dependencies. Programmed custom .NET
  functionality for SQL Server 2008 and PostgreSQL, leveraging the latest tools
  in spatial database technologies. Processed complex, nationwide geospatial
  layers in spatial databases using set-based logic for increased speed with
  spatially-enabled SQL.
|

| **Geoprocessor For Elevation**, National Cartography and Geospatial Center
  (*USDA/Paradigm Systems GIS Inc*)
| Designed and built a complete GIS for offering geospatial mapping services
  and geoprocessing services to remote field offices. The web services can be
  accessed from a variety of clients via a REST interface provided by ArcGIS
  Server. Two client-side implementations were built, one in ArcMap and the
  other in Microsoft Silverlight for use in a web browser. In either
  environment, a user selects an area-of-interest, along with other parameters,
  to extract subset data from massive elevation datasets to produce derivative
  products (i.e. hillshade, slope, contours). The architecture allows for more
  efficient dissemination of data. 
|

| **Elevation Initiative**, National Cartography and Geospatial Center
  (*USDA/Paradigm Systems GIS Inc*)
| Coded ArcGIS geoprocessing services for allowing on-the-fly access to
  high-resolution Digital Elevation Models (DEMs) and selectable derivative
  products, including custom contours, hillshades, slope rasters, and aspect
  rasters.
|

| **LiDAR Processing**, National Cartography and Geospatial Center
  (*USDA/Paradigm Systems GIS Inc*)
| Developed a system for effectively cataloging, indexing, projecting, and
  processing thousands LIDAR datasets from LAS point cloud format to Digital
  Elevation Models (DEMs) for consumption in any GIS application using a
  combination of open source tools and custom-coded software. 
|

| **Conservation Delivery Streamlining Initiative (CDSI) Mobile Pilot**,
  National Cartography and Geospatial Center (*USDA/Paradigm Systems GIS Inc*) 
| Architected and deployed an ArcGIS Mobile pilot project for collecting field
  data with handheld GPS devices. The solution allowed for both
  direct-connected synchronization of geospatial data over mobile networks and
  disconnected editing where network signals were unavailable. 
|

| **GeoObserver for Easements**, National Cartography and Geospatial Center
  (*USDA/Paradigm Systems GIS Inc*)
| A nationwide web-based mapping application for monitoring government-owned
  property is currently in the process of being moved to a production
  environment. The project uses Microsoft's Silverlight platform in conjunction
  with the ESRI Silverlight API. The result is a fast, reliable, and secure
  interactive map that runs in a user's web browser. For this project I created
  scalable map services, assisted in creating a SQL Server 2008 backend with
  the new spatial types, and provided C# code for a WCF service to provide the
  spatial data to the Silverlight application. I developed code for a "swipe"
  tool commonly used in change detection between various years of aerial
  imagery. Example code was posted on the ESRI code library and has received
  very positive feedback from the geospatial community. 
|
 
| **National Hydrography Dataset (NHD)**, National Cartography and Geospatial
  Center (*USDA/Paradigm Systems GIS Inc*)
| Imported massive NHD spatial datasets from ESRI ArcSDE export files as
  Microsoft SQL Server 2008 native geometry types for additional spatial
  functionality. Integrated the .NET Common Language Runtime (CLR) and
  developed user-defined aggregates (UDAs) in C# for storing one-to-many
  relationships in a single column with a specified delimiter for a legacy
  processing system. Created Stored Procedures (SPs), including advanced
  procedures for finding "nearest neighbor" that utilized SQL Server 2008
  spatial indexes for increased performance. Built triggers in SQL to automate
  attribution of tabular data based on spatial relationships, making the
  process transparent to individuals involved in data entry. 
|
 
| **MapServerMonitor**, National Cartography and Geospatial Center
  (*USDA/Paradigm Systems GIS Inc*)
| Designed and programmed an application to monitor ESRI map services and
  source layers for administrative purposes. The application was written in the
  Python programming language as a cross-platform solution to common problems
  related to distributed network environments, where web services are hard to
  track. This piece of software provides control to a system administrator,
  allowing the administrator to automate the process of hunting down
  non-functioning ArcGIS services. 
|
 
| **GPS Statistical Analysis Project**, National Cartography and Geospatial
  Center (*USDA/Paradigm Systems GIS Inc*)
| Gigabytes of proprietary, flat-file GPS point cloud data were parsed and
  processed into PostGIS, an extension to the PostgreSQL database that allows
  for the storage and manipulation of spatial data. Several custom spatial SQL
  functions were employed to process the data spatially, and the R statistical
  environment was incorporated to perform descriptive statistical analyses on
  the GPS data for the creation of a final report that assessed unit accuracy.
|
 
| **Historic Resources Survey**, URS Corporation / LopezGarcia Group 
| Contributed to a cultural resources survey for the City of Fort Worth. The
  project included surveys of viable economic areas, or "urban villages", and
  creating GIS-layered documentation for use by the City's planning and zoning
  departments. Over 2,000 resources were surveyed. I was responsible for the
  extraction, consolidation, and organization of field-collected GPS data. I
  devised and implemented a method for interactively viewing photographs of
  historic resources in an ArcGIS desktop mapping environment. Prior to the
  final report submission I also contributed to the digitization of historic
  neighborhood boundaries used in cartographic display.
|
 
| **Ables Springs 12138kV Transmission Line**, URS Corporation / LopezGarcia
  Group
| This project included mapping existing and future environmental constraints;
  identifying alternative routes; and identifying and evaluating the impacts of
  the alternative and preferred routes. The study area contained habitable
  structures; potential areas of species diversity, including a native and
  restored prairie; prime farmland; geology; and potential cultural resources.
  These services were performed in accordance with 37.056(c)(4)(A)-(D) of the
  Texas Utilities Code, Commission Procedural Rule 22.52(a)(4), Commission
  Substantive Rule 25.101(b)(3)(B) and the Commission's policy of prudent
  avoidance. My primary role dealt with providing guidance with technical
  aspects regarding quantification of geographic features. For the final
  document I consolidated data from a distributed database into a single report.
|
 
| **The T Southwest to Northeast Rail Corridor EIS/PE**, URS
  Corporation / LopezGarcia Group
| Assisted in geographic information systems data collection and analysis for
  The T's Southwest to Northeast Rail Corridor EIS/PE for a proposed commuter
  rail line extending from southwest Fort Worth to Dallas/Fort Worth
  International Airport (DFW Airport). Existing conditions and environmental
  impacts, including land uses, air and water quality, floodplains, threatened
  and endangered species, wetlands, hazardous materials and environmental
  justice, were analyzed in GIS. Maps, figures and presentation boards were
  generated in GIS using the results of this analysis. A historic-age structure
  survey of more than 4,000 buildings using global positioning system (GPS)
  equipment in concert with GIS was required on this project. I created maps
  from templates and layouts; assisted in identifying the proposed rail
  alignment, project area, right-of-way, potential hazardous sites, soil
  classification and land use information.
|
 
| **DWU Eastside Water Treatment Plant (WTP) to Southwest Dallas Water
  Transmission Pipeline**, URS Corporation / LopezGarcia Group
| Participated in the preparation of several map products associated with the
  preliminary investigation of the recommended alignment corridor for a
  proposed water transmission pipeline. The pipeline is anticipated to consist
  of approximately 16 miles of 120-inch pipeline and 16 miles of 96-inch
  pipeline. This project scope included preparation of field maps for
  environmental impact and cultural resource studies and data analysis of
  demographics and land usage. All deliverables were completed in ArcGIS and
  incorporated into the DWU Digital Resource Library. 
|
 
| **East End Corridor Metro**, URS Corporation / LopezGarcia Group
| Was responsible for data collection and analysis of park lands and community
  service facilities during the preparation of a Draft Environmental Impact
  Assessment (DEIA). The proposed project involved the construction of a fixed
  Guided Rapid Transit (GRT) corridor primarily within existing roadway
  rights-of-way and the METRO owned Westpark railroad right-of-way. I worked
  closely with the planning department to provide data needed for reports. 
