#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Allan Adair'
SITENAME = 'Allan Adair'
SITEURL = 'http://allan.adair.io'
DISQUS_SITENAME = 'allanadair'

PLUGIN_PATHS = ['../pelican-plugins']
PLUGINS = ['pelican_javascript']

PATH = 'content'
STATIC_PATHS = ['images', 'js', 'css', 'extra/favicon.ico', 'white_rock_lake']
EXTRA_PATH_METADATA = {'extra/favicon.ico': {'path': 'favicon.ico'}}
ARTICLE_PATHS = ['Blog']

# Theme stuff
THEME = 'theme2'
MENUITEMS = (('allan.adair.io', '/'),
             ('About', '/pages/about.html'),
             ('CV', '/pages/curriculum-vitae.html'),
             ('Miscellaneous Code', '/pages/miscellaneous-code.html'))
DISPLAY_PAGES_ON_MENU = False

TIMEZONE = 'America/Tijuana'

DEFAULT_LANG = 'en'
USE_FOLDER_AS_CATEGORY = True
DEFAULT_DATE = 'fs'
DEFAULT_METADATA = {'authors': 'Allan Adair',
                    'status': 'draft'}

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = 4

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
